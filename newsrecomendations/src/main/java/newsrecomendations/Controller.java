package newsrecomendations;

import io.micronaut.http.annotation.Get;
import io.reactivex.Flowable;

import java.util.List;

@io.micronaut.http.annotation.Controller("/news")
public class Controller {
    private final NewsInventoryClient newsInventoryClient;
    private  final NewsCatalogueClient newsCatalogueClient;

    public Controller (NewsCatalogueClient newsCatalogueClient, NewsInventoryClient newsInventoryClient){
        this.newsCatalogueClient = newsCatalogueClient;
        this.newsInventoryClient = newsInventoryClient;
    }


    //Empty Body Exception (Keine Ahnung warum)
    @Get("/findAll")
    public List<News> FindAllController(){
        List<News> debug = newsCatalogueClient.findAll();
        return newsCatalogueClient.findAll();
    }

    //Funktioniert
    @Get("/stock")
    public Boolean StockController(){
        return newsInventoryClient.stock("13");
    }
}
