package newsrecomendations;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;

import javax.validation.constraints.NotBlank;

@Client("http://localhost:8082")
public interface NewsInventoryClient{

    @Get("/news/stock/{title}")
    Boolean stock(@NotBlank String title);

}
