package newsrecomendations;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

import java.util.List;

@Client("http://localhost:8081")
public interface NewsCatalogueClient{

    @Get("/newsTitles")
    List<News> findAll();
}
