package newsinventory;


import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import io.micronaut.validation.Validated;

import javax.validation.constraints.NotBlank;
import java.util.logging.Logger;

@Validated
@io.micronaut.http.annotation.Controller("/news")
public class Controller {
    @Produces(MediaType.TEXT_PLAIN)
    @Get("/stock/{title}")
    public Boolean isInStock(@NotBlank String title){
        return SearchByTitle(title);
    }

    private Boolean SearchByTitle(String title){
        if(title.equals("13"))
            return true;
        else
            return false;
    }
}
