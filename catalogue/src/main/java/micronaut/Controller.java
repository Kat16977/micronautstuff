package micronaut;

import io.micronaut.http.annotation.Get;

import java.util.Arrays;
import java.util.List;

@io.micronaut.http.annotation.Controller("/newsTitles")
public class Controller {
    @Get("/")
    List<News> index(){
        News news0 = new News("Artikel 11 und 13");
        News news1 = new News("Trump immer noch President");
        News news2 = new News("Pewdiepie bleibt Nr. 1");
        News news3 = new News("YouTube hat jetzt konkurenz!");
        return Arrays.asList(news0,news1,news2,news3);
    }
}
